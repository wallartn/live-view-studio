defmodule LiveViewStudioWeb.LightLive do
  use LiveViewStudioWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, assign(socket, :brightness, 10)}
  end

  def handle_event("on", _, socket) do
    {:noreply, assign(socket, :brightness, 100)}
  end

  def handle_event("off", _, socket) do
    {:noreply, assign(socket, :brightness, 0)}
  end

  def handle_event("up", _, socket) do
    # On utilise update pour ceux-ci une méthode plus parlante pour le même résultat
    # On utilise une fonction pour le dernier paramètre
    {:noreply, update(socket, :brightness, fn b -> min(b + 10, 100) end)}
  end

  def handle_event("down", _, socket) do
    # On utilise la syntaxe de capture, même résultat toujours
    {:noreply, update(socket, :brightness, &max(&1 - 10, 0))}
  end

  def render(assigns) do
    ~H"""
    <h1>Front Porch Light</h1>
    <div id="light">
      <div class="meter">
        <span style={"width: #{@brightness}%"}>
          <%= @brightness %>%
        </span>
      </div>
    </div>
    <button phx-click="off">
      <.icon name="hero-light-bulb" />
    </button>
    <button phx-click="down">
      <.icon name="hero-chevron-down" />
    </button>

    <button phx-click="up">
      <.icon name="hero-chevron-up" />
    </button>
    <button phx-click="on">
      <.icon name="hero-light-bulb-solid" />
    </button>
    """
  end
end
